package lection_7_threds;

/**
 * Created by Slava on 20.12.2016.
 */
public class TestStatic {

    public static void main(String[] args) {
        StaticHolder sh = new StaticHolder();
        StaticHolder sh1 = new StaticHolder();

        sh.name = "Artem";
        sh.surName = "Test";
        System.out.println(sh.name);
        System.out.println(sh.surName);
        System.out.println(sh1.name);
        System.out.println(sh1.surName);
    }
}
