package lection_7_threds.method1_1;

public class Program {

    public static void main(String[] args) throws InterruptedException {
        //Создание потока
        Thread myThready = new Thread(() -> {
            while(true) {
                System.out.println("Привет из побочного потока!");
            }
        });
        myThready.start();    //Запуск потока

        myThready.join();

        System.out.println("Главный поток завершён...");
    }
}
