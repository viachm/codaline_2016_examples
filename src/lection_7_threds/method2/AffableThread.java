package lection_7_threds.method2;

class AffableThread extends Thread {

    @Override
    public void run()    //Этот метод будет выполнен в побочном потоке
    {
        System.out.println("Привет из побочного потока!");
    }
}
