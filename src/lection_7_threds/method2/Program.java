package lection_7_threds.method2;

public class Program {

    static AffableThread mSecondThread;

    public static void main(String[] args) {
        mSecondThread = new AffableThread();    //Создание потока
        mSecondThread.start();                    //Запуск потока

        System.out.println("Главный поток завершён...");
    }
}